{ config, lib, pkgs, ... }:
{
  imports =
    [
      ./hardware-configuration.nix
      ./hosts.nix
    ];

  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/sda";

  networking.hostName = "art";
  networking.networkmanager.enable = true;

  time.timeZone = "America/Toronto";

  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    #keyMap = "us";
    useXkbConfig = true; # use xkb.options in tty.
  };

  # Enable sound.
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    wireplumber.enable = true;
    # media-session.enable = false;
    jack.enable = true;
    systemWide = false;
  };

  users.users.kdam0 = {
    isNormalUser = true;
    extraGroups = [ 
      "wheel" 
      "networkmanager" 
      "audio" 
      "video" 
      "docker"
      "libvirtd"
    ];
    packages = with pkgs; [
      tree
    ];
    shell = pkgs.zsh;
  };

  environment.systemPackages = with pkgs; [
    vim
    neovim
    wget
    git
    rsync
    htop
    bmon
    alsa-utils
    docker-buildx
    docker-compose
    wireguard-tools
    cacert
    openssl.dev
    (python3.withPackages (ps: with ps; [
        pip
        regex
        pyopenssl
    ]))
  ];

  virtualisation.docker = {
    enable = true;
    enableOnBoot = true;
    rootless = {
      enable = true;
      setSocketVariable = true;
    };
  };

  programs.mtr.enable = true;
  programs.gnupg.agent = {
    enable = true;
    enableSSHSupport = true;
  };
  programs.zsh.enable = true;
  programs.hyprland.enable = true;

  # List services that you want to enable:
  services.printing.enable = true;
  services.openssh.enable = true;
  services.thermald.enable = true;
  services.tlp.enable = true;
  services.logind.lidSwitch = "suspend-then-hibernate";
  services.gvfs.enable = true;
  #services.gnome.gnome-keyring.enable = true;

  security.polkit.enable = true;
  security.pam.services = {
    login.gnupg.enable = true;
    #gdm.enableGnomeKeyring = true;
    #hyprland.enableGnomeKeyring = true;
  };

  powerManagement.enable = true;

  networking.firewall.enable = false;

  systemd.sleep.extraConfig = ''
    AllowSuspend=yes
    AllowHybridSleep=no
    AllowHibernation=yes
    AllowSuspendThenHibernate=yes
    HibernateDelaySec=1m
  '';

  nix.settings.experimental-features = ["nix-command" "flakes"];
  nix.settings.auto-optimise-store = true;

  system.stateVersion = "23.11"; # DO NOT CHANGE
  system.autoUpgrade.enable = true;
  system.autoUpgrade.allowReboot = true;
}
