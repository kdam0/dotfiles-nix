{ config, pkgs, ...}: 
{
  home.username = "kdam0";
  home.homeDirectory = "/home/kdam0";
  home.sessionPath = [
        "$HOME/.config/nixpkgs/common/scripts"
  ];
  home.sessionVariables = {
	EDITOR = "nvim";
	TERM = "xterm-256color";
  };
  home.shellAliases = {
      l = "ls -l";
      ll = "ls -al";
      ".." = "cd ..";
      v = "nvim $@";
      r = "ranger $@";
      gst = "git status";
      chm = "nvim ~/.config/nixpkgs/per-user/kdam0-home/home.nix";
      clsp = "nvim ~/.config/nixpkgs/common/nvim/plugins/lspconfig.lua";
      cnix = "nvim ~/.config/nixpkgs/per-user/kdam0-home/configuration.nix";
      up = "sudo nixos-rebuild switch";
      clean = "sudo nix-collect-garbage -d && nix-collect-garbage -d";
      search = "nix search nixpkgs $@";
      conf = "ranger ~/.config/nixpkgs/common/";
  };

  programs.home-manager.enable = true;
  programs.zsh = {
    enable = true;
    defaultKeymap = "viins";
    initExtra = ''
      source ${config.home.homeDirectory}/.p10k.zsh
      bindkey '^ ' autosuggest-accept
    '';
    zplug = {
      enable = true;
      plugins = [
        { name = "zsh-users/zsh-autosuggestions"; } 
        { name = "romkatv/powerlevel10k"; tags = [ as:theme depth:1 ]; } 
      ];
    };
  };
  programs.tmux = {
    enable = true;
    clock24 = true;
    shell = "${pkgs.zsh}/bin/zsh";
    prefix = "C-a";
    terminal = "screen-256color";
    keyMode = "vi";
    plugins = with pkgs; [
      tmuxPlugins.sensible
      {
        plugin = tmuxPlugins.resurrect;
        extraConfig = "set -g @resurrect-strategy-nvim 'session'";
      }
      tmuxPlugins.pain-control
      tmuxPlugins.yank
    ];
    sensibleOnTop = true;
  };
  programs.git = {
    enable = true;
    userName  = "Kumar Damani";
    userEmail = "me@kumardamani.net";
    aliases = {
      lg = "log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit";
    };
  };

  programs.neovim = {
    enable = true;
    withPython3 = true;
    withRuby = true;
    withNodeJs = true;
    vimdiffAlias = true;
    plugins = with pkgs.vimPlugins; [
      tokyonight-nvim
      nvim-treesitter.withAllGrammars
      nvim-autopairs
      nvim-cmp
      cmp-buffer
      cmp-path
      nvim-web-devicons
      nnn-vim
      luasnip
      cmp_luasnip
      cmp-nvim-lsp
      friendly-snippets
      {
        plugin = indent-blankline-nvim-lua;
	type = "lua";
  	config = builtins.readFile(../../common/nvim/plugins/indent-blankline.lua);
      }
      {
        plugin = lualine-nvim;
	type = "lua";
  	config = builtins.readFile(../../common/nvim/plugins/lualine.lua);
      }
      {
        plugin = trouble-nvim;
	type = "lua";
  	config = builtins.readFile(../../common/nvim/plugins/trouble.lua);
      }
      {
        plugin = telescope-nvim;
	type = "lua";
  	config = builtins.readFile(../../common/nvim/plugins/telescope.lua);
      }
      telescope-fzf-native-nvim
      {
        plugin = leap-nvim;
	type = "lua";
  	config = builtins.readFile(../../common/nvim/plugins/leap.lua);
      }
      mason-nvim
      mason-lspconfig-nvim
      neoconf-nvim
      {
        plugin = nvim-lspconfig;
	type = "lua";
  	config = builtins.readFile(../../common/nvim/plugins/lspconfig.lua);
      }
    ];
    extraConfig = builtins.readFile ../../common/nvim/init.vim;
  };


  home.stateVersion = "23.05";
  home.packages = [
    # Web
    pkgs.firefox
    pkgs.brave
    # Image Editor
    pkgs.krita
    # Fonts
    pkgs.nerdfonts
    pkgs.hack-font
    pkgs.noto-fonts
    pkgs.noto-fonts-cjk
    # Latex stuff
    (pkgs.texlive.combine { inherit (pkgs.texlive) scheme-small xifthen ifmtarg framed paralist titling titlesec libertine letltxmacro; })
    pkgs.texmaker
    pkgs.gnumake
    pkgs.clang
    pkgs.neofetch
    pkgs.ripgrep
    pkgs.fd
    pkgs.unzip
    pkgs.nodejs
    pkgs.cargo
    # Files
    pkgs.ranger
    pkgs.nnn
    pkgs.vlc
    # Comms
    pkgs.signal-desktop
    pkgs.mutt-wizard
    pkgs.isync
    pkgs.msmtp
    pkgs.pass
    pkgs.neomutt
    pkgs.nextcloud-client
    pkgs.thunderbird
    # Office stuff
    pkgs.libreoffice-qt
    pkgs.hunspell
    pkgs.hunspellDicts.en_CA
    pkgs.lynx
    # dev stuff
    pkgs.terraform
    pkgs.ansible
    pkgs.jq
    pkgs.publii
    pkgs.s3fs
  ];

  fonts.fontconfig.enable = true;

  services.gpg-agent = {
    enable = true;
    defaultCacheTtl = 1800;
    enableSshSupport = true;
  };

  systemd.user.startServices = true;

  # Source app specific configs
  home.file.".local/share/fonts".source = ../../common/fonts;
}
