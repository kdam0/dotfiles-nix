local telescope = require('telescope')
local actions = require('telescope.actions')

local trouble = require("trouble.providers.telescope")

telescope.setup {
  defaults = {
    file_ignore_patterns = { "^.git/" },
    mappings = {
      i = { ["<c-o>"] = trouble.open_with_trouble },
      n = { ["<c-o>"] = trouble.open_with_trouble },
    },
  },
}

telescope.load_extension('fzf')
