```bash
stow -v -t ~ nix nixpkgs
```

OrbStack usage:
1. Create a debian 12 machine with the name `orb-deb` and user `kdma0` (default).
2. Specify the follwing `cloud-init.yaml`:
```yaml
#cloud-config

users:
  - name: kdam0
    homedir: /home/kdam0
    sudo: ["ALL=(ALL) NOPASSWD:ALL"]
    groups: users, admin, docker

groups:
  - docker

package_update: true
packages:
  - curl
  - git
  - stow
  - xz-utils
  - locales-all

write_files:
- content: |
    #!/bin/bash
    HOME="/home/kdam0" sh <(curl -L https://nixos.org/nix/install) --daemon --yes
  path: /run/installer
  permissions: '0755'

runcmd:
  - curl -fsSL https://get.docker.com | sh
  - /run/installer
  - su - kdam0 -c 'git clone "https://gitlab.com/kdam0/dotfiles-nix.git"'
  - su - kdam0 -c 'cd dotfiles-nix && stow -v -t ~ nix home-manager && git remote set-url origin git@gitlab.com:kdam0/dotfiles-nix.git'
  - su - kdam0 -c 'nix run home-manager/master switch'
  - chsh -s /home/kdam0/.nix-profile/bin/zsh kdam0
  - echo "machine is ready for use!"
```
