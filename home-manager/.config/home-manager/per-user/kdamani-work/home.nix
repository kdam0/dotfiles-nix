{ config, pkgs, lib, ...}: 
{
  home.shellAliases = {
    up = lib.mkForce "cd ~/.config/home-manager && darwin-rebuild switch --flake .";
    ssh = "INVOKED_VIA_ALIAS=yes ~/.ssh/config.d/managed/scripts/renew.sh";
  };
  home.packages = with pkgs; [
    colima
    docker
    kubectl
    k9s
    step-cli
    teleport
  ];
  programs.git.userEmail = lib.mkForce "kdamani@kepler.space";
}
