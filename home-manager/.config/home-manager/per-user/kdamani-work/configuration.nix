{ config, pkgs, ... }:

{
  users.users.kdamani.home = "/Users/kdamani";

  environment.systemPackages =
    [ pkgs.vim
    ];

  # Auto upgrade nix package and the daemon service.
  # services.nix-daemon.enable = true;
  nix.package = pkgs.nix;

  # The default Nix build user group ID was changed from 30000 to 350.
  # but this system was built using the old value
  ids.gids.nixbld = 30000;

  # Create /etc/zshrc that loads the nix-darwin environment.
  programs.zsh.enable = true;  # default shell on catalina

  # Used for backwards compatibility, please read the changelog before changing.
  # $ darwin-rebuild changelog
  system.stateVersion = 5;
}
