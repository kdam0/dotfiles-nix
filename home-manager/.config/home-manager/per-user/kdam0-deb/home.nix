{ config, pkgs, libs, ...}: 
{
  home.username = "kdam0";
  home.homeDirectory = "/home/kdam0";
  home.sessionPath = [
  	"$HOME/.config/home-manager/common/scripts"
  ];
  home.sessionVariables = {
    EDITOR = "nvim";
    TERM = "xterm-256color";
  };
  home.shellAliases = {
    l = "ls -l";
    ll = "ls -al";
    ".." = "cd ..";
    v = "nvim $@";
    r = "ranger $@";
    gst = "git status";
    chm = "nvim ~/.config/home-manager/per-user/kdam0-deb/home.nix";
    cvim = "nvim ~/.config/home-manager/common/nvim/init.vim";
    clsp = "nvim ~/.config/home-manager/common/nvim/plugins/lspconfig.lua";
    search = "nix search home-manager $@";
    conf = "ranger ~/.config/home-manager/common/";
    up = "home-manager switch";
  };

  programs.home-manager.enable = true;
  programs.zsh = {
    enable = true;
    defaultKeymap = "viins";
    initExtra = ''
      source ${config.home.homeDirectory}/.p10k.zsh
      bindkey '^ ' autosuggest-accept
    '';
    zplug = {
      enable = true;
      plugins = [
        { name = "zsh-users/zsh-autosuggestions"; } 
        { name = "romkatv/powerlevel10k"; tags = [ as:theme depth:1 ]; } 
      ];
    };
  };
  programs.tmux = { 
    enable = true;
    clock24 = true;
    shell = "${pkgs.zsh}/bin/zsh";
    prefix = "C-a";
    terminal = "screen-256color";
    keyMode = "vi";
    plugins = with pkgs; [
      tmuxPlugins.sensible
      {
        plugin = tmuxPlugins.resurrect;
        extraConfig = "set -g @resurrect-strategy-nvim 'session'";
      }
      tmuxPlugins.pain-control
      tmuxPlugins.yank
    ];
    sensibleOnTop = true;
  };
  programs.git = {
    enable = true;
    userName  = "Kumar Damani";
    userEmail = "me@kumardamani.net";
    aliases = {
      lg = "log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit";
    };
  };
  programs.neovim = {
    enable = true;
    withPython3 = false;
    withRuby = true;
    withNodeJs = true;
    vimdiffAlias = true;
    plugins = with pkgs.vimPlugins; [
      tokyonight-nvim
      nvim-treesitter.withAllGrammars
      nvim-autopairs
      nvim-cmp
      cmp-buffer
      cmp-path
      nvim-web-devicons
      nnn-vim
      luasnip
      cmp_luasnip
      cmp-nvim-lsp
      friendly-snippets
      {
        plugin = indent-blankline-nvim-lua;
        type = "lua";
        config = builtins.readFile(../../common/nvim/plugins/indent-blankline.lua);
      }
      {
        plugin = lualine-nvim;
        type = "lua";
        config = builtins.readFile(../../common/nvim/plugins/lualine.lua);
      }
      {
        plugin = trouble-nvim;
        type = "lua";
        config = builtins.readFile(../../common/nvim/plugins/trouble.lua);
      }
      {
        plugin = telescope-nvim;
        type = "lua";
        config = builtins.readFile(../../common/nvim/plugins/telescope.lua);
      }
      telescope-fzf-native-nvim
      {
        plugin = leap-nvim;
        type = "lua";
        config = builtins.readFile(../../common/nvim/plugins/leap.lua);
      }
      mason-nvim
      mason-lspconfig-nvim
      neoconf-nvim
      {
        plugin = nvim-lspconfig;
        type = "lua";
        config = builtins.readFile(../../common/nvim/plugins/lspconfig.lua);
      }
    ];
    extraConfig = builtins.readFile ../../common/nvim/init.vim;
  };
  programs.fzf = {
    enable = true;
    tmux.enableShellIntegration = true;
  };
  programs.direnv = {
    enable = true;
    enableZshIntegration = true; # see note on other shells below
    nix-direnv.enable = true;
  };
  home.stateVersion = "23.05";
  home.packages = with pkgs; [
    ranger
    nnn
    ripgrep
    python311
  ];
  targets.genericLinux.enable = true;
}
