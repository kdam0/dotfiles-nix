{ config, pkgs, libs, ...}: 
{
  home.username = "kdam0";
  home.homeDirectory = "/home/kdam0";
  home.sessionPath = [
  	"$HOME/.config/home-manager/common/scripts"
  ];
  home.sessionVariables = {
    EDITOR = "hx";
    TERM = "xterm-256color";
  };
  home.shellAliases = {
    l = "ls -l";
    ll = "ls -al";
    ".." = "cd ..";
    v = "$EDITOR $@";
    r = "yazi $@";
    gst = "git status";
    gd = "git diff";
    chm = "$EDITOR ~/.config/home-manager/per-user/kdam0-orb-deb/home.nix";
    search = "nix search nixpkgs $@";
    conf = "ranger ~/.config/home-manager/common/";
    up = "home-manager switch";
    tmux = "tmux -u $@";
  };

  programs.home-manager.enable = true;
  programs.zsh = {
    enable = true;
    defaultKeymap = "viins";
    initExtra = ''
      source ${config.home.homeDirectory}/.p10k.zsh
      bindkey '^ ' autosuggest-accept
    '';
    zplug = {
      enable = true;
      plugins = [
        { name = "zsh-users/zsh-autosuggestions"; } 
        { name = "romkatv/powerlevel10k"; tags = [ as:theme depth:1 ]; } 
      ];
    };
  };
  programs.tmux = { 
    enable = true;
    clock24 = true;
    shell = "${pkgs.zsh}/bin/zsh";
    prefix = "C-a";
    terminal = "xterm-256color";
    keyMode = "vi";
    plugins = with pkgs; [
      tmuxPlugins.sensible
      {
        plugin = tmuxPlugins.resurrect;
        extraConfig = "set -g @resurrect-strategy-nvim 'session'";
      }
      tmuxPlugins.pain-control
      tmuxPlugins.yank
    ];
    sensibleOnTop = true;
  };
  programs.git = {
    enable = true;
    userName  = "Kumar Damani";
    userEmail = "me@kumardamani.net";
    aliases = {
      lg = "log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit";
    };
    delta = {
      enable = true;
    };
  };
  programs.fzf = {
    enable = true;
    tmux.enableShellIntegration = true;
  };
  programs.direnv = {
    enable = true;
    enableZshIntegration = true; # see note on other shells below
    nix-direnv.enable = true;
  };
  programs.helix = {
    enable = true;
    settings = {
      # theme = "jetbrains_dark";
      theme = "gruvbox_dark_hard";
      editor = {
        mouse = false;
        true-color = true;
        line-number = "relative";
        lsp.display-messages = true;
        rulers = [81];
        indent-guides = {
          render = true;
          character = "╎";
        };
        cursor-shape = {
          normal = "block";
          insert = "bar";
          select = "block";
        };
      };
      keys.normal = {
        space.space = "file_picker";
        space.w = ":w";
        space.q = ":q";
        G = "goto_file_end";
        esc = [ "collapse_selection" "keep_primary_selection" ];
      };
    };
    extraPackages = [
      pkgs.bash-language-server
      pkgs.marksman
      pkgs.gopls
      pkgs.ansible-language-server
      pkgs.yamllint
      pkgs.ruff
      pkgs.pyright
      pkgs.pylyzer
    ];
    languages = {
      language-server.ruff = {
        command = "ruff";
        args = ["server"];
        config = {
          settings = {
            lineLength = 88;
            lint = {
              select = ["B" "C4" "I" "N" "PIE" "RUF" "TCH" "TID" "UP" "S" "W" "E" "F"];
              preview = false;
            };
            format = {
              preview = true;
            };
          };
        };
      };
      language-server.pyright = {
        config.python.analysis = {
          typeCheckingMode = "basic";
        };
      };
      language-server.pylyzer = {
        command = "pylyzer";
        config = {
          settings = {
            args = ["--server"];
          };
        };
      };
      language = [
        {
          name = "python";
          language-servers = ["ruff" "pyright" "pylyzer"];
          auto-format = true;
        }
      ];
    };
  };
  home.stateVersion = "23.05";
  home.packages = with pkgs; [
    dig
    fd
    htop
    imagemagick
    jq
    poppler
    python311
    ripgrep
    yazi
  ];
  targets.genericLinux.enable = true;
}
