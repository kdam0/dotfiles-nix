{ config, pkgs, libs, ...}: 
{
  home.username = "kdam0";
  home.homeDirectory = "/home/kdam0";
  targets.genericLinux.enable = true;
}
