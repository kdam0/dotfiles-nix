{ config, pkgs, ... }:

{ networking.extraHosts = ''
# Your /etc/hosts entries go here  
127.0.0.2 other-localhost

192.168.0.87 art-jr
'';
}
