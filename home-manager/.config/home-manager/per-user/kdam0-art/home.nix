{ config, pkgs, libs, ...}: 
{
  home.username = "kdam0";
  home.homeDirectory = "/home/kdam0";
  home.sessionPath = [
  	"$HOME/.config/nixpkgs/common/scripts"
  ];
  home.sessionVariables = {
    EDITOR = "nvim";
    TERM = "xterm-256color";
  };
  home.shellAliases = {
    l = "ls -l";
    ll = "ls -al";
    ".." = "cd ..";
    v = "nvim $@";
    r = "ranger $@";
    gst = "git status";
    cnix = "nvim ~/.config/nixpkgs/per-user/kdam0-art/configuration.nix";
    chm = "nvim ~/.config/nixpkgs/per-user/kdam0-art/home.nix";
    cvim = "nvim ~/.config/nixpkgs/common/nvim/init.vim";
    clsp = "nvim ~/.config/nixpkgs/common/nvim/plugins/lspconfig.lua";
    search = "nix search nixpkgs $@";
    conf = "ranger ~/.config/nixpkgs/common/";
    up = "cd ~/.config/nixpkgs; sudo nixos-rebuild switch --flake .";
  };

  programs.home-manager.enable = true;
  programs.zsh = {
    enable = true;
    defaultKeymap = "viins";
    initExtra = ''
      source ${config.home.homeDirectory}/.p10k.zsh
      bindkey '^ ' autosuggest-accept
    '';
    zplug = {
      enable = true;
      plugins = [
        { name = "zsh-users/zsh-autosuggestions"; } 
        { name = "romkatv/powerlevel10k"; tags = [ as:theme depth:1 ]; } 
      ];
    };
  };
  programs.tmux = { 
    enable = true;
    clock24 = true;
    shell = "${pkgs.zsh}/bin/zsh";
    prefix = "C-a";
    terminal = "screen-256color";
    keyMode = "vi";
    plugins = with pkgs; [
      tmuxPlugins.sensible
      {
        plugin = tmuxPlugins.resurrect;
        extraConfig = "set -g @resurrect-strategy-nvim 'session'";
      }
      tmuxPlugins.pain-control
      tmuxPlugins.yank
    ];
    sensibleOnTop = true;
  };
  programs.git = {
    enable = true;
    userName  = "Kumar Damani";
    userEmail = "me@kumardamani.net";
    aliases = {
      lg = "log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit";
    };
  };
  programs.neovim = {
    enable = true;
    withPython3 = false;
    withRuby = true;
    withNodeJs = true;
    vimdiffAlias = true;
    plugins = with pkgs.vimPlugins; [
      tokyonight-nvim
      nvim-treesitter.withAllGrammars
      nvim-autopairs
      nvim-cmp
      cmp-buffer
      cmp-path
      nvim-web-devicons
      nnn-vim
      luasnip
      cmp_luasnip
      cmp-nvim-lsp
      friendly-snippets
      {
        plugin = indent-blankline-nvim-lua;
        type = "lua";
        config = builtins.readFile(../../common/nvim/plugins/indent-blankline.lua);
      }
      {
        plugin = lualine-nvim;
        type = "lua";
        config = builtins.readFile(../../common/nvim/plugins/lualine.lua);
      }
      {
        plugin = trouble-nvim;
        type = "lua";
        config = builtins.readFile(../../common/nvim/plugins/trouble.lua);
      }
      {
        plugin = telescope-nvim;
        type = "lua";
        config = builtins.readFile(../../common/nvim/plugins/telescope.lua);
      }
      telescope-fzf-native-nvim
      {
        plugin = leap-nvim;
        type = "lua";
        config = builtins.readFile(../../common/nvim/plugins/leap.lua);
      }
      mason-nvim
      mason-lspconfig-nvim
      neoconf-nvim
      {
        plugin = nvim-lspconfig;
        type = "lua";
        config = builtins.readFile(../../common/nvim/plugins/lspconfig.lua);
      }
    ];
    extraConfig = builtins.readFile ../../common/nvim/init.vim;
  };
  programs.fzf = {
    enable = true;
    tmux.enableShellIntegration = true;
  };
  programs.direnv = {
    enable = true;
    enableZshIntegration = true; # see note on other shells below
    nix-direnv.enable = true;
  };

  programs.waybar.enable = true;
  home.stateVersion = "23.11";
  nixpkgs = {
    config = {
      allowUnfree = true;
      allowUnfreePredicate = (_: true);
    };
  };

  home.packages = with pkgs; [
    # Window environment
    xdg-desktop-portal-hyprland
    foot
    wl-clipboard
    wf-recorder
    grimblast
    wbg
    # Browsers
    firefox
    brave
    # File Managers
    ranger
    nnn
    # Fonts
    noto-fonts
    noto-fonts-emoji
    noto-fonts-monochrome-emoji
    nerdfonts
    hack-font
    font-awesome
    # Notifications
    mako
    libnotify
    # Image viewer
    imv
    pinta
    # Video player
    mpv
    # Pdf Viewer
    zathura
    # Power stuff
    acpi
    # Other stuff
    entr
    gnumake
    neofetch
    trash-cli
    nextcloud-client
    ripgrep
    fd
    stow
    unzip
    nodejs
    cargo
    lazygit
    signal-desktop
    wev
    sshpass
    libreoffice
    drawio
    virt-viewer
    dig
    dnsutils
    s3fs
    jq
    publii
    thunderbird
    wofi
    pavucontrol
    pcmanfm
    gnome.seahorse
  ];

  fonts.fontconfig.enable = true;

  services.gpg-agent = {
    enable = true;
    defaultCacheTtl = 1800;
    enableSshSupport = true;
  };
  services.gnome-keyring.enable = true;
  services.gnome-keyring.components = ["secrets"];

  systemd.user.services = {
    bg-wallpaper = {
      Unit = {
        Description = "Auto refresh wallpaper";
        After = "network-online.target";
      };
      Service = {
        Type = "simple";
        ExecStart = "${config.home.homeDirectory}/.config/nixpkgs/common/scripts/walp";
        TimeoutStopSec = "10";
        KillMode = "process";
        KillSignal = "SIGKILL";
      };
      Install.WantedBy = ["multi-user.target"];
    };
  };
  systemd.user.timers = {
    bg-wallpaper = {
      Unit.Description = "Automatic update of wallpaper";
      Timer.OnUnitActiveSec = "10min";
      Install.WantedBy = ["multi-user.target" "timers.target"];
    };
  };
  systemd.user.startServices = true;

  wayland.windowManager.hyprland = {
    enable = true;
    settings = {
      "$mod" = "ALT";
      input = {
        "kb_options" = "caps:swapescape";
      };
      monitor = [
        "DP-1, 1920x1080@60, 0x0,1"
        ", preferred,auto,1"
      ];
      workspace = [
        "1,monitor:eDP-1"
        "2,monitor:DP-1"
        "3,monitor:DP-1"
        "4,monitor:DP-1"
        "5,monitor:DP-1"
        "6,monitor:DP-1"
        "7,monitor:DP-1"
        "8,monitor:DP-1"
        "9,monitor:DP-1"
        "10,monitor:DP-1"
      ];
      windowrulev2 = [
        "workspace 9,class:^firefox$"
        "workspace 8,class:^Brave(.*)$"
        "workspace 7,class:^remote-viewer$"
        "workspace 1,class:^Slack$"
        "workspace 1,class:^Signal$"
      ];
      animation = [
        "workspaces,1,2,default"
        "windows,1,3,default"
        "fade,0"
      ];
      bind = [
          "$mod, j, cyclenext, prev"
          "$mod Shift, j, swapnext, prev"
          "$mod, k, cyclenext,"
          "$mod Shift, k, swapnext,"
          "$mod Shift, l, exec, swaylock -i /tmp/bg"
          "$mod, period, focusmonitor, +1"
          "$mod, Tab, workspace, previous"
          "$mod, escape, workspace, m-1"
          "$mod, semicolon, workspace, m+1"
          "$mod, q, killactive"
          "$mod, Return, exec, foot"
          "$mod, p, exec, wofi --show run"
          "$mod, r, exec, foot -e ranger"
          ", Delete, exec, grimblast copy area"
          "Shift, Delete, exec, GRIMBLAST_EDITOR=pinta grimblast edit area"
        ]
        ++ (
          # workspaces
          # binds $mod + [shift +] {1..10} to [move to] workspace {1..10}
          builtins.concatLists (builtins.genList (
            x: let
              ws = let
                c = (x + 1) / 10;
              in
                builtins.toString (x + 1 - (c * 10));
              in [
                "$mod, ${ws}, workspace, ${toString (x + 1)}"
                "$mod SHIFT, ${ws}, movetoworkspace, ${toString (x + 1)}"
              ]
            )
          10)
        );
      binde = [
        ", XF86AudioMute, exec, wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle"
        "$mod, Next, exec, wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%-"
        "$mod, Prior, exec, wpctl set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ 5%+"
        ", XF86MonBrightnessDown, exec, light -U 10"
        ", XF86MonBrightnessUp, exec, light -A 10"
        "$mod, h, resizeactive, -20 20"
        "$mod, l, resizeactive, 20 -20"
      ];
      bindm = [
          "$mod,mouse:272,movewindow"
          "$mod,mouse:273,resizewindow"
      ];
      binds = {
        "allow_workspace_cycles" = true;
      };
      "exec-once" = [
        "${pkgs.polkit_gnome}/libexec/polkit-gnome-authentication-agent-1"
        #"${pkgs.plasma5Packages.polkit-kde-agent}/libexec/polkit-kde-authentication-agent-1"
        "mako"
        "walp"
        "waybar"
        "[workspace 9 silient] firefox"
        "[workspace 2 silent] foot"
        "[workspace 1 silient] slack"
        "[workspace 1 silient] signal-desktop"
      ];
    };
  };


  # Source app specific configs
  home.file.".config/foot/foot.ini".source = ../../common/foot/foot.ini;
  home.file.".config/wofi/style.css".source = ../../common/wofi/style.css;
  home.file.".config/mako/config".source = ../../common/mako/config;
  home.file.".local/share/fonts".source = ../../common/fonts;
  home.file.".config/waybar".source = ../../common/waybar;

  #home.file.".config/foot/foot.ini".text = ''
  #[colors]
  #alpha=0.7
  #'';
}
