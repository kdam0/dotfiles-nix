local telescope = require('telescope')
local actions = require('telescope.actions')

local trouble = require("trouble.sources.telescope")

telescope.setup {
  defaults = {
    file_ignore_patterns = { "^.git/" },
    mappings = {
      i = { ["<c-o>"] = trouble.open },
      n = { ["<c-o>"] = trouble.open },
    },
  },
}

telescope.load_extension('fzf')
