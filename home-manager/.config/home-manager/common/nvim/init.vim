nnoremap <SPACE> <Nop>
let mapleader = " "
colorscheme tokyonight-moon
set number relativenumber
set cursorline

" Keymaps
" save
nmap <Leader>, :w<cr>
" exit to normal mode
inoremap jk <Esc>
" Toggle line chars view
noremap <Leader>l :set list!<CR>

" Telescope Keymaps
map <Leader><Space> :Telescope find_files<CR>
map <Leader>/ :Telescope live_grep<CR>
map <Leader>g :Telescope git_files<CR>
map <Leader>: :Telescope command_history<CR>

" Indent blocks in visual mode.
vnoremap K xkP`[V`]
vnoremap J xp`[V`]
vnoremap L >gv
vnoremap H <gv

" Move beteween splits
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

" Start n³ in the current buffers directory
let g:nnn#action = {
      \ '<c-t>': 'tab split',
      \ '<c-s>': 'split',
      \ '<c-v>': 'vsplit' }
nnoremap <Leader>n :NnnPicker %:p:h<CR>

autocmd BufRead,BufNewFile *.yaml set filetype=yaml.ansible
