{config, lib, pkgs, ...}:
{
  nix.gc.automatic = true;

  home = {
    sessionPath = [
    	"$HOME/.config/home-manager/common/scripts"
    ];
    sessionVariables = {
      EDITOR = "hx";
    };
    shellAliases = {
      l = "ls -l";
      ll = "ls -al";
      ".." = "cd ..";
      v = "$EDITOR $@";
      r = "yazi $@";
      gst = "git status";
      gd = "git diff";
      search = "nix search nixpkgs $@";
      conf = "$EDITOR ~/.config/home-manager/common/default.nix";
      up = "cd ~/.config/home-manager && home-manager switch";
      tmux = "tmux -u $@";
    };
    stateVersion = "23.05";
    packages = with pkgs; [
      coreutils
      dig
      fd
      htop
      imagemagick
      jq
      nmap
      poppler
      python311
      ripgrep
      smartcat
      sshpass
      tldr
      wget
      yazi
    ];
  };

  programs = {
    home-manager.enable = true;
    zsh = {
      enable = true;
      defaultKeymap = "viins";
      initExtra = ''
        source ${config.home.homeDirectory}/.p10k.zsh
        bindkey '^ ' autosuggest-accept
      '';
      zplug = {
        enable = true;
        plugins = [
          { name = "zsh-users/zsh-autosuggestions"; } 
          { name = "romkatv/powerlevel10k"; tags = [ as:theme depth:1 ]; } 
        ];
      };
    };
    tmux = { 
      enable = true;
      clock24 = true;
      shell = "${pkgs.zsh}/bin/zsh";
      prefix = "C-a";
      terminal = "xterm-256color";
      keyMode = "vi";
      plugins = with pkgs; [
        tmuxPlugins.sensible
        {
          plugin = tmuxPlugins.resurrect;
          extraConfig = "set -g @resurrect-strategy-nvim 'session'";
        }
        tmuxPlugins.pain-control
        tmuxPlugins.yank
      ];
      sensibleOnTop = true;
      extraConfig = "set -g default-command '$SHELL'";
    };
    git = {
      enable = true;
      userName  = "Kumar Damani";
      userEmail = "me@kumardamani.net";
      aliases = {
        lg = "log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit";
      };
      delta = {
        enable = true;
      };
      lfs = {
        enable = true;
      };
      extraConfig = {
        push = { autoSetupRemote = true; };
      };
    };
    fzf = {
      enable = true;
      tmux.enableShellIntegration = true;
    };
    direnv = {
      enable = true;
      enableZshIntegration = true; # see note on other shells below
      nix-direnv.enable = true;
      config = {
        global = {
          load_dotenv = true;
        };
      };
    };
    helix = {
      enable = true;
      settings = {
        # theme = "jetbrains_dark";
        theme = "gruvbox_dark_hard";
        editor = {
          mouse = false;
          true-color = true;
          line-number = "relative";
          lsp.display-messages = true;
          rulers = [81];
          indent-guides = {
            render = true;
            character = "╎";
          };
          cursor-shape = {
            normal = "block";
            insert = "bar";
            select = "block";
          };
          file-picker = {
            hidden = false;
          };
          end-of-line-diagnostics = "hint";
          inline-diagnostics = {
            cursor-line = "error";
          };
        };
        keys.normal = {
          space.space = "file_picker";
          space.w = ":w";
          space.q = ":q";
          G = "goto_file_end";
          esc = [ "collapse_selection" "keep_primary_selection" ];
        };
      };
      extraPackages = [
        pkgs.bash-language-server
        pkgs.marksman
        pkgs.gopls
        pkgs.ansible-language-server
        pkgs.yaml-language-server
        pkgs.ruff
        pkgs.pyright
        pkgs.pylyzer
        pkgs.nodePackages.prettier
        pkgs.typescript-language-server
        pkgs.vscode-langservers-extracted
      ];
      languages = {
        language-server.ruff = {
          command = "ruff";
          args = ["server"];
          config = {
            settings = {
              lineLength = 88;
              lint = {
                select = ["B" "C4" "I" "N" "PIE" "RUF" "TCH" "TID" "UP" "S" "W" "E" "F"];
                preview = false;
              };
              format = {
                preview = true;
              };
            };
          };
        };
        language-server.pyright = {
          config.python.analysis = {
            typeCheckingMode = "basic";
          };
        };
        language-server.pylyzer = {
          command = "pylyzer";
          config = {
            settings = {
              args = ["--server"];
            };
          };
        };
        language = [
          {
            name = "python";
            language-servers = ["ruff" "pyright" "pylyzer"];
            auto-format = true;
          }
          {
            name = "javascript";
            language-servers = ["typescript-language-server"];
            formatter = {
              command = "prettier";
              args = [ "--parser" "typescript" ];
            };
            auto-format = true;
          }
        ];
      };
    };
  };
}
