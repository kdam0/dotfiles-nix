{
  description = "NixOS configuration";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    darwin.url = "github:lnl7/nix-darwin";
    darwin.inputs.nixpkgs.follows = "nixpkgs";
    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
    # nixos-hardware.url = "github:NixOS/nixos-hardware/master";
  };

  outputs = inputs@{ nixpkgs, darwin, home-manager, ... }: {
    # NixOS + Home Manager
    nixosConfigurations = {
      "art-sr" = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          ./per-user/kdam0-home/configuration.nix
          home-manager.nixosModules.home-manager
          {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
            home-manager.users.kdam0 = import ./per-user/kdam0-home/home.nix;

            # Optionally, use home-manager.extraSpecialArgs to pass
            # arguments to home.nix
          }
        ];
      };
    };
    # Darwin + Home Manager
    darwinConfigurations = {
      "Kumar-Damanis-MacBook-Air" = darwin.lib.darwinSystem {
        system = "aarch64-darwin";
        modules = [
          ./per-user/kdamani-work/configuration.nix
          home-manager.darwinModules.home-manager
          {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
            home-manager.users.kdamani = {
              imports = [
                ./common
                ./per-user/kdamani-work/home.nix
              ];
            };
          }
        ];
      };
    };

    # Home Manager Only
    homeConfigurations = {
      "kdam0" = home-manager.lib.homeManagerConfiguration {
        pkgs = nixpkgs.legacyPackages."x86_64-linux";
        modules = [
          ./common
          ./per-user/kdam0-wsl/home.nix
        ];

        # Optionally use extraSpecialArgs
        # to pass through arguments to home.nix
      };
    };
  };
}
